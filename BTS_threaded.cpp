#include<iostream>
#include<ostream>
using namespace std;

typedef int KEY;
typedef string T;

struct Elem {
KEY key;
T data;
Elem *left;
Elem *right;
bool rightThread; //normal right child link or a threadlink
};

Elem *_root; // a dummy root sentinel

int _size;
// helper method for inserting record into tree.

bool  insert(Elem *& root, const KEY &key, const T &data, Elem *lastLeft);
// helper method for print tree

void printTree(ostream& out, int level, Elem *p);
// common code for deallocation

void destructCode(Elem *& p);

bool insert(Elem *& root, const KEY &key, const T &data, Elem *lastLeft){
    Elem *tmp = new Elem;
    Elem *ptr = root;
    Elem *parent = NULL; 
    
    tmp -> key = key;
    tmp -> rightThread = false;
    tmp -> data = data;
    tmp -> left = NULL;
    tmp -> right = NULL;
    if(root==NULL){
    	tmp -> rightThread = true;
    	root = tmp;
    	return true;
	}
    while (true)
    {
        parent = ptr;
        
        if (key == (ptr->key))
        {
            printf("The key already exist\n");
            return false;
        }
 
        
 
        
        if (key < ptr->key)
        {   
            
			   ptr = ptr -> left;
            if(ptr==NULL)
               {
               	parent -> left = tmp;
				tmp-> right = parent;
				tmp->rightThread = true; 
				return true;
            
        }
}
        else
        {
            if (ptr->rightThread == false)
                {
                	
			        	ptr = ptr -> right;
			        if(ptr==NULL){
			        	parent->right = tmp;
			        	return false;
					}
				
				}
            else
                {
                	Elem *tmp1 = ptr -> right;
                	ptr -> right = tmp;
                	tmp->right = tmp1;
                	tmp->rightThread = true;
                	return true;
        }
    }
	}

    
}
struct Elem *leftMost( Elem *ptr)
{   
Elem *current = ptr;
    if(ptr==NULL){
    	return NULL;
	}
	else{
		while(current->left!=NULL){
		
			current = current -> left;
		}
		return current;
	}
}
void printTree( int level, Elem *p){
	if (p == NULL)
        printf("Tree is empty");
 
    struct Elem *ptr = leftMost(p);

  
    while (ptr != NULL)
    {
        cout<<ptr -> data<<"\t";
        
        if(ptr->rightThread){
        	ptr = ptr->right;
		}
        else{
        	ptr = leftMost(ptr->right);
		}
        
    }
}
void destructCode(Elem *& p){
	delete(p);
}
int main(){
	insert(_root,10,"10",_root);
	_size = 1;
	insert(_root,20,"20",_root);
	_size ++;
	insert(_root,30,"30",_root);
	_size ++;
	insert(_root,40,"40",_root);
	_size ++;
	insert(_root,50,"50",_root);
	_size ++;
	insert(_root,25,"25",_root);
	_size ++;
	printTree(_size,_root);
	return 1;
}
