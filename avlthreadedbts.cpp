#include<iostream>
using namespace std;
typedef int KEY;
typedef string T;

struct Elem {
Elem():left(0), right(0), height(-1), rightThread(false) {}
KEY key;
T data;
Elem *left;
Elem *right;
int height;
bool rightThread; //normal right child link or a threadlink
};
Elem *_root; // a dummy root sentinel
int _size;
// helper method for inserting record into tree.
bool insert(Elem *& root, const KEY &key, const T &data, Elem *lastLeft);

// helper method for print tree
void printTree(ostream& out, int level, Elem *p);
// common code for deallocation
void destructCode(Elem *& p);
void rotateRight(Elem *& node);
void rotateLeft(Elem *& node);
void doubleRotateRight(Elem *& node);
void doubleRotateLeft(Elem *& node);
int balanceFactor(Elem *cur);
void balance(Elem*& cur, const KEY &key);
int height(Elem *node);
void updateHeight(Elem*& cur);


void rotateRight(Elem *& node)
{
    Elem *x = node->left;
    Elem *T2 = x->right;
    Elem *y = node;
    
     if(T2->rightThread){
    	x->right->rightThread = true;
    	x->right->right = x;
    	T2->rightThread = false;
	}
    x->right = y;
    
    y->left = T2;
   
 
    
    y->height = max(height(y->left),
                    height(y->right)) + 1;
    x->height = max(height(x->left),
                    height(x->right)) + 1;
 

    node = x;
}
 

void rotateLeft(Elem *& node)
{
    Elem *y = node->right;
    Elem *T2 = y->left;
    Elem *x = node;
    y->left = x;
    
    if(y->rightThread){
    	y->rightThread = false;
        T2->right = y;
        T2->rightThread = true;
	}
    y->right = T2;
    
    
    x->height = max(height(x->left),   
                    height(x->right)) + 1;
    y->height = max(height(y->left),
                    height(y->right)) + 1;
 
    
    node = y;
}
 
 
void balance(Elem*& cur, const KEY &key){
	int balance = balanceFactor(cur);
   
    if (balance > 1 && key < cur->left->key)
       rotateRight(cur);

    if (balance < -1 && key > cur->right->key)
        rotateLeft(cur);
 
    if (balance > 1 && key > cur->left->key)
    {
       rotateLeft(cur->left);
        rotateRight(cur);
    }
 
    if (balance < -1 && key < cur->right->key)
    {   
        rotateRight(cur->right);
       rotateLeft(cur);
    }
}

int height(Elem *node){
	if(node == NULL)
	 return 0;
	return node->height;
}
int balanceFactor(Elem *cur){
	if (cur == NULL)
        return 0;
    return height(cur->left) - height(cur->right);
}
bool insert(Elem *& root, const KEY &key, const T &data, Elem *lastLeft){
    Elem *tmp = new Elem;
    Elem *ptr = root;
    Elem *parent = NULL; 
    
    tmp -> key = key;
    tmp -> rightThread = false;
    tmp -> data = data;
    tmp -> left = NULL;
    tmp -> right = NULL;
    if(root==NULL){
    	tmp -> rightThread = true;
    	root = tmp;
    	return true;
	}
    while (true)
    {
        parent = ptr;
        
        if (key == (ptr->key))
        {
            printf("The key already exist\n");
            return false;
        }
 
        
 
        
        if (key < ptr->key)
        {   
            if(ptr->left != NULL)
			   ptr = ptr -> left;
            else
               {
               	parent -> left = tmp;
				tmp-> right = parent;
				tmp->rightThread = true; 
				tmp->height = 1 + max(height(tmp->left),height(tmp->right));
				balance(parent,key);
				root = parent;
				return true;
            
        }
}
        else
        {
            if (ptr->rightThread == false)
                {
                	if(ptr->right != NULL)
			        	ptr = ptr -> right;
			        else{
			        	parent->right = tmp;
			        	tmp->height = 1 + max(height(tmp->left),height(tmp->right));
			        balance(parent,key);
			        root = parent;
			        	return false;
					}
				
				}
            else
                {
                	Elem *tmp1 = ptr -> right;
                	ptr -> right = tmp;
                	tmp->right = tmp1;
                	tmp->rightThread = true;
                	tmp->height = 1 + max(height(tmp->left),height(tmp->right));
                	balance(parent,key);
                	root = parent;
                	return true;
        }
    }
	}

    
}
 Elem *leftMost( Elem *ptr)
{   
Elem *current = ptr;
    if(ptr==NULL){
    	return NULL;
	}
	else{
		while(current->left!=NULL){
		
			current = current -> left;
		}
		return current;
	}
}
 printTree( int level, Elem *p){
	if (p == NULL)
        printf("Tree is empty");
 
    struct Elem *ptr = leftMost(p);

  
    while (ptr != NULL)
    {
        cout<<ptr -> data<<"\t";
        
        if(ptr->rightThread){
        	ptr = ptr->right;
		}
        else{
        	ptr = leftMost(ptr->right);
		}
        
    }
}

void destructCode(Elem *& p){
	delete(p);
}
int main(){
	insert(_root,10,"10",_root);
	_size = 1;
	insert(_root,20,"20",_root);
	_size ++;
	insert(_root,30,"30",_root);
	_size ++;
	printTree(_size,_root);
	return 1;
}
